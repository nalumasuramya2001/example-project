package com.project.service;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.PassengersTicket;
import com.project.model.Train;
import com.project.respository.PassengerTicketrepository;
import com.project.serviceinterface.PassengersTicketserviceinterface;

@Service
public class PassengersTicketservice implements PassengersTicketserviceinterface {
	
	@Autowired
	PassengerTicketrepository passengerrepository;


	public ModelAndView addpassengerticketrequest() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("passengerticket.jsp");
	return mview;
	}
    public ModelAndView addpassengerticket(PassengersTicket passengerticket) {
	ModelAndView mview=new ModelAndView();
	passengerrepository.save(passengerticket);
	mview.setViewName("redirect:/addanotherticket.jsp");
	return mview;
	}
	

    public ModelAndView ticketlistreq() {
	ModelAndView mview =new ModelAndView();
	List<PassengersTicket> list=passengerrepository.findAll();
	mview.addObject("list", list);
	mview.setViewName("ticketview.jsp");
	return mview;
    }
    public ModelAndView getticketlist(PassengersTicket passengerticket) {
    ModelAndView mview =new ModelAndView();
    mview.setViewName("redirect:/");
    return mview;
    }

	public ModelAndView dltticketreq() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("passengerticketdlt.jsp");
	return mview;
	}
    public ModelAndView dltticket(Integer passengerid) {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("redirect:/ticketdltdone.jsp");
	passengerrepository.deleteById(passengerid);
	return mview;
	}
}
