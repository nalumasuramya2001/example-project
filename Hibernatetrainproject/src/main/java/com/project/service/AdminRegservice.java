package com.project.service;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.ModelAndView;
import com.project.model.AdminReg;
import com.project.model.Train;
import com.project.respository.AdminRegrepository;
import com.project.serviceinterface.AdminRegserviceinterface;
@Service
public class AdminRegservice implements AdminRegserviceinterface{

@Autowired
AdminRegrepository adminregrepo;
	


   public ModelAndView adminregistrationrequest() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("adminreg.jsp");
	return mview;
    }
    public  ModelAndView addadminregistration(AdminReg  adminreg) {
	ModelAndView mv= new ModelAndView();
	adminregrepo.save( adminreg);
    mv.setViewName("redirect:/adminlogin.jsp");
	return mv;
    }



     public ModelAndView adminlogrequest() {
	 ModelAndView mview=new ModelAndView();
	 mview.setViewName("adminlogin.jsp");
	 return mview;
     }
     public ModelAndView adminlogchecking(String emailid,String password) {
     ModelAndView mview= new ModelAndView();
     AdminReg adminreg=getadmin(emailid,password);
	 if(emailid.equals(adminreg.getEmailid()) && password.equals(adminreg.getPassword())) {
     adminregrepo.findByemailidpassword(emailid,password);
	 mview.setViewName("redirect:/adminoperations.jsp");
     } 
     return mview;
     }
	public AdminReg getadmin(String emailid,String password) {
	return adminregrepo.findByemailid(emailid,password);
	}
	
     



}
