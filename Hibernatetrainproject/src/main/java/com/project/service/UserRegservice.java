package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.AdminReg;
import com.project.model.UserReg;
import com.project.respository.UserRegrepository;
import com.project.serviceinterface.UserRegserviceinterface;
@Service
public class UserRegservice implements UserRegserviceinterface{
@Autowired
UserRegrepository userregrepo;
	
    public ModelAndView userregistrationrequest() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("userreg.jsp");
	return mview;
    }
    public ModelAndView adduserreg(UserReg ur) {
	ModelAndView mview=new ModelAndView();
	userregrepo.save(ur);
    mview.setViewName("redirect:/userlogin.jsp");
    return mview;
    }
    
    



    public ModelAndView userlogrequest() {
	 ModelAndView mview=new ModelAndView();
	 mview.setViewName("userlogin.jsp");
	 return mview;
    }
    public ModelAndView userlogchecking(String emailid,String password) {
	 ModelAndView mview= new ModelAndView();
	 UserReg ur=getuser(emailid,password);
	 if(emailid.equals(ur.getEmailid()) && password.equals(ur.getPassword())) {
	 userregrepo.findByuseremailidpassword(emailid,password);
	 mview.setViewName("redirect:/reservenew.jsp");
	 }
	 return mview;
	 }
    
    public UserReg getuser(String emailid,String password) {
    return userregrepo.findByemailid(emailid,password);
    }

   

}
