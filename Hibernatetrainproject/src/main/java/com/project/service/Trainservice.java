package com.project.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.AdminReg;
import com.project.model.Train;

import com.project.respository.Trainrepository;
import com.project.serviceinterface.Trainserviceinterface;
@Service
public class Trainservice implements Trainserviceinterface{
@Autowired
Trainrepository trainrepo;
   

    public ModelAndView totaltrainlistrequest() {
	ModelAndView mview =new ModelAndView();
	List<Train> listtrain=trainrepo.findAll();
	mview.addObject("listtrain", listtrain);
	mview.setViewName("trainlistview.jsp");
	return mview;
    }
    public ModelAndView gettrainlist(Train train) {
    ModelAndView mview =new ModelAndView();
    mview.setViewName("redirect:/selectmembers.jsp");
    return mview;
    }

    
      
	public ModelAndView addtrainrequest() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("Trainadd.jsp");
	return mview;
	}
	public ModelAndView addtrain(Train tn) {
	ModelAndView mview=new ModelAndView();	
	trainrepo.save(tn);
	mview.setViewName("redirect:/traindone.jsp");
    return mview;
	}



	public ModelAndView deletetrainrequest() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("traindlt.jsp");
	return mview;
	}
    public ModelAndView dlttrain(Integer trainno) {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("redirect:/traindltdone.jsp");
	trainrepo.deleteById(trainno);
	return mview;
	}
	
    
    public ModelAndView trainupdaterequest() {
    ModelAndView mview=new ModelAndView();
	mview.setViewName("trainupdate.jsp");
	return mview;
	}
    public ModelAndView updatetrain(Train tn) {
	Train existingdata=trainrepo.findById(tn.getTrainno()).orElse(null);
	existingdata.setTrainname(tn.getTrainname());
	existingdata.setSource(tn.getSource());
	existingdata.setDestination(tn.getDestination());
	existingdata.setJourneystarttime(tn.getJourneystarttime());
	existingdata.setSeattype(tn.getSeattype());
	existingdata.setAc1seatcost(tn.getAc1seatcost());
	existingdata.setAc2seatcost(tn.getAc2seatcost());
	existingdata.setSleeper_seatcost(tn.getSleeper_seatcost());
	existingdata.setGeneral_seatcost(tn.getGeneral_seatcost());
	trainrepo.save(existingdata);
	ModelAndView mview=new ModelAndView();	
	mview.setViewName("redirect:/trainupdatedone.jsp");
	return mview;
	}
    
    
    
	public ModelAndView request4() {
	ModelAndView mview=new ModelAndView();
    mview.setViewName("reservenew.jsp");
	return mview;
	}
	public ModelAndView findBysourcedestination(String source,String destination) {
	 ModelAndView mview=new ModelAndView();	
	 trainrepo.findBysourcedestination(source,destination);
	 mview.setViewName("redirect:/totaltrainlistrequest");
	 
	 return mview;
	}
	
	
	


}
