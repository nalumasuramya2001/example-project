package com.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.NewPayment;

import com.project.respository.NewPaymentrepository;
import com.project.serviceinterface.NewPaymentserviceinterface;

@Service
public class NewPaymentservice implements NewPaymentserviceinterface{
	@Autowired
	NewPaymentrepository paymentrepository;

	
	
	public ModelAndView paymentrequest() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("paymentnew.jsp");
	return mview;
	}
	public ModelAndView addpayment(NewPayment newpayment) {
	ModelAndView mview=new ModelAndView();
	paymentrepository.save(newpayment);
	mview.setViewName("redirect:/done.jsp");
	return mview;
	}

}
