package com.project.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.Members;
import com.project.respository.Memberrepository;
import com.project.serviceinterface.Memberserviceinterface;

@Service
public class Memberservice implements Memberserviceinterface{

	@Autowired
    Memberrepository memberrepo;

    

     public ModelAndView membersrequest() {
     ModelAndView mview=new ModelAndView();
     mview.setViewName("selectmembers.jsp");
     return mview;
	 }
     public ModelAndView addmembers(Members members) {
     ModelAndView mview=new ModelAndView();
     mview.setViewName("redirect:/passengerticket.jsp");
     memberrepo.save(members);
     return mview;
	}

	


}
