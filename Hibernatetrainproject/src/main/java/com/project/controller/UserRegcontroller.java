package com.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.AdminReg;
import com.project.model.UserReg;
import com.project.service.AdminRegservice;
import com.project.service.UserRegservice;

@RestController
public class UserRegcontroller {
	@Autowired
	UserRegservice userregservice;
	
	
	@RequestMapping("userregistrationrequest")
	public ModelAndView userregistrationrequest() {
	return	userregservice.userregistrationrequest();
	}
	@PostMapping("adduserregistration")
	public ModelAndView adduserreg(@ModelAttribute UserReg userreg) {
	return userregservice.adduserreg(userreg);
	}

	
	
	

	@RequestMapping("userlogrequest")
	public ModelAndView userlogrequest() {
		return userregservice.userlogrequest();
	}
     @GetMapping("userlogin")   
     public ModelAndView userlogchecking(@RequestParam(value = "emailid")String emailid,@RequestParam(value = "password")String password) {
    	 return userregservice.userlogchecking(emailid,password);
	}
}
