package com.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.NewPayment;
import com.project.service.NewPaymentservice;


@RestController
public class NewPaymentcontroller {

	@Autowired
	NewPaymentservice paymentservice;
	
	
	@RequestMapping("paymentrequest")
	public ModelAndView paymentrequest(){
	return paymentservice.paymentrequest();
	}
	@PostMapping("/addpaymentforticket")
	public ModelAndView addpayment(NewPayment newpayment){
	return paymentservice.addpayment(newpayment);
	}

}
