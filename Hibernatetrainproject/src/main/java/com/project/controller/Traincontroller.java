package com.project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.project.model.Train;
import com.project.respository.Trainrepository;
import com.project.service.Trainservice;


@RestController
public class Traincontroller {
	@Autowired
	Trainservice trainservice;
	
	@Autowired
	Trainrepository trainrepo;

	

	
	
    @RequestMapping("totaltrainlistrequest")
    public ModelAndView totaltrainlistrequest() {
    return	trainservice.totaltrainlistrequest();
    }
    @GetMapping("/gettrainlist")
    public ModelAndView gettrainlist(@ModelAttribute Train train) {
    return trainservice.gettrainlist(train);
    }

     
    @RequestMapping("addtrainrequest")
	public ModelAndView addtrainrequest() {
	return	trainservice.addtrainrequest();
	}
	@PostMapping("addtrain")
	public ModelAndView addtrain(@ModelAttribute Train tn) {
		return trainservice.addtrain(tn);
	}
	
	
	@RequestMapping("deletetrainrequest")
	public ModelAndView deletetrainrequest() {
	return	trainservice.deletetrainrequest();
	}
    @DeleteMapping("canceltrain")
	public ModelAndView dlttrain(@RequestParam(defaultValue = "0")Integer trainno) {
    return trainservice.dlttrain(trainno);
	
    }
	

	@RequestMapping("trainupdaterequest")
	public ModelAndView trainupdaterequest() {
	return	trainservice.trainupdaterequest();
	}
    @PutMapping("updatetrain")
	public ModelAndView updatetrain(Train tn) {
		return trainservice.updatetrain(tn);
	}
	
	
    @RequestMapping("requestparticulartrains") 
	public ModelAndView request4() {
		return trainservice.request4();
	}
    @GetMapping( "/searchtrains")   
    public ModelAndView findBysourcedestination(@RequestParam(value = "source")String source,@RequestParam(value = "destination")String destination) {
    return trainservice.findBysourcedestination(source,destination);

	}
   
    
 
   
}
