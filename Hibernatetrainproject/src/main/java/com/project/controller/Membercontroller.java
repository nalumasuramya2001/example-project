package com.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.Members;
import com.project.respository.Memberrepository;
import com.project.service.Memberservice;

@RestController
public class Membercontroller {

	@Autowired
    Memberservice memberservice;
    


     @RequestMapping("membersrequest")
     public ModelAndView membersrequest() {
	 return memberservice.membersrequest();
     }
     @PostMapping("addmembers")
     public ModelAndView addmembers(@ModelAttribute Members members) {
	 return memberservice.addmembers(members);
     }



} 