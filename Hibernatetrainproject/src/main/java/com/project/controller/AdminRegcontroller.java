package com.project.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.project.model.AdminReg;
import com.project.model.Train;
import com.project.respository.AdminRegrepository;
import com.project.service.AdminRegservice;

@RestController
public class AdminRegcontroller {
	@Autowired
	AdminRegservice adminRegservice;
	
	@Autowired
	AdminRegrepository adminregrepo;
	
	@RequestMapping("contact")
    public ModelAndView contact() {
	ModelAndView mview=new ModelAndView();
	mview.setViewName("contact.jsp");
	return mview;
	}
	@RequestMapping("Home")
    public ModelAndView display1() {
	ModelAndView mview=new ModelAndView(); 
	mview.setViewName("Homenew.jsp");
	return mview;
	}

	
	
	@RequestMapping("adminregrequest")
	public ModelAndView adminregistrationrequest() {
	return	adminRegservice.adminregistrationrequest();
	}
	@PostMapping("addadmin")
	public ModelAndView addadminregistration(@ModelAttribute AdminReg adminreg) {
    return adminRegservice.addadminregistration(adminreg);
	}
	

	
	
	@RequestMapping("adminlogrequest")
	public ModelAndView adminlogrequest() {
		return adminRegservice.adminlogrequest();
	}
     @GetMapping( "adminlogin")   
     public ModelAndView adminlogchecking(@RequestParam(value = "emailid")String emailid,@RequestParam(value = "password")String password) {
     return adminRegservice.adminlogchecking(emailid,password);
     }
     

}
