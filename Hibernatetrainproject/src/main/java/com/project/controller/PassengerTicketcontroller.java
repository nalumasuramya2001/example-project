package com.project.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.project.model.PassengersTicket;
import com.project.model.Train;

import com.project.service.PassengersTicketservice;

@RestController
public class PassengerTicketcontroller {
	
	@Autowired
	PassengersTicketservice passengerservice;
	

	@RequestMapping("passengersticketrequest")
	public ModelAndView addpassengerticketrequest() {
	return	passengerservice.addpassengerticketrequest();
	}
	@PostMapping("addpassengerticket")
	public ModelAndView addpassengerticket(@ModelAttribute PassengersTicket passengerticket) {
	return passengerservice.addpassengerticket(passengerticket);
	}

	
	
    @RequestMapping("ticketlistreq")
    public ModelAndView ticketlistreq() {
    return	 passengerservice.ticketlistreq();
    }
    @GetMapping("getticketlist")
    public ModelAndView getticketlist(@ModelAttribute  PassengersTicket passengerticket) {
    return passengerservice.getticketlist(passengerticket);
    }
    
    
	@RequestMapping("dltticketreq")
	public ModelAndView dltticketreq(){
	return passengerservice.dltticketreq();
	}
    @DeleteMapping("cancelticket")
	public ModelAndView dltticket(@RequestParam(defaultValue = "0")Integer passengerid) {
    return  passengerservice.dltticket(passengerid);
	
    }


}
