package com.project.serviceinterface;

import org.springframework.web.servlet.ModelAndView;

import com.project.model.NewPayment;

public interface NewPaymentserviceinterface {

	
	public ModelAndView paymentrequest();
	public ModelAndView addpayment(NewPayment newpayment);
}


