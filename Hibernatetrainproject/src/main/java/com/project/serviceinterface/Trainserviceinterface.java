package com.project.serviceinterface;

import java.util.List;

import org.springframework.web.servlet.ModelAndView;

import com.project.model.Train;

public interface Trainserviceinterface {

	
	//public ModelAndView totaltrainlistrequest();
	public ModelAndView gettrainlist(Train train);
	
	
	public ModelAndView addtrainrequest();
	public ModelAndView addtrain(Train tn);
	
	
	public ModelAndView deletetrainrequest();
	public ModelAndView dlttrain(Integer trainno);
	
	public ModelAndView trainupdaterequest();
	public ModelAndView updatetrain(Train tn);
	
	
	public ModelAndView request4();
	public ModelAndView findBysourcedestination(String source,String destination);
}
