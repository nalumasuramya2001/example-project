package com.project.serviceinterface;

import org.springframework.web.servlet.ModelAndView;

import com.project.model.PassengersTicket;

public interface PassengersTicketserviceinterface {

	public ModelAndView addpassengerticketrequest();
	public ModelAndView addpassengerticket(PassengersTicket passengerticket);
	
	
	 public ModelAndView ticketlistreq();
	 public ModelAndView getticketlist(PassengersTicket passengerticket) ;
	 
	 
	 public ModelAndView dltticketreq();
	 public ModelAndView dltticket(Integer passengerid);
}
