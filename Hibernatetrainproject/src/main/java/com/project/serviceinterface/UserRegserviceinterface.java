package com.project.serviceinterface;

import org.springframework.web.servlet.ModelAndView;

import com.project.model.UserReg;

public interface UserRegserviceinterface {

	
	public ModelAndView userregistrationrequest();
	public ModelAndView adduserreg(UserReg ur);
	
	
	 public ModelAndView userlogrequest();
	 public ModelAndView userlogchecking(String emailid,String password);
}
