package com.project.serviceinterface;

import org.springframework.web.servlet.ModelAndView;

import com.project.model.AdminReg;

public interface AdminRegserviceinterface  {

	 public ModelAndView adminregistrationrequest();
	 public  ModelAndView addadminregistration(AdminReg  adminreg);
	 
	 
	 public ModelAndView adminlogrequest();
	 public ModelAndView adminlogchecking(String emailid,String password);
}
