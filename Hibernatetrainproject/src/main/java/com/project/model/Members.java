package com.project.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Members {
@Id	
int Adults;
int Children;
public int getAdults() {
	return Adults;
}
public void setAdults(int adults) {
	Adults = adults;
}
public int getChildren() {
	return Children;
}
public void setChildren(int children) {
	Children = children;
}
public Members(int adults, int children) {
	super();
	Adults = adults;
	Children = children;
}
public Members() {
	super();
}
@Override
public String toString() {
	return "Members [Adults=" + Adults + ", Children=" + Children + "]";
}

}
