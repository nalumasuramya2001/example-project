package com.project.model;

import java.util.Scanner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

@Entity
public class UserReg {
	    @Id
	    @Column
	    Integer userregid;
	    String user_fullname;
	    @Column
	    @Size(message="@ is mandatory")
	    String emailid;
		long mobileno;
		@Column
		@Size(min=8,message="The Password Operation at least 8 characters")
        @Size(max=15,message="The Password Operation at most 15 characters")
		String password;
		@Size(min=8,message="The Password Operation at least 8 characters")
	    @Size(max=15,message="The Password Operation at most 15 characters")
		@Column
		String conformpassword;
		String address;
	    String gender;
		

		public Integer getUserregid() {
			return userregid;
		}
		public void setUserregid(Integer userregid) {
			this.userregid = userregid;
		}
		public String getUser_fullname() {
			return user_fullname;
		}
		public void setUser_fullname(String user_fullname) {
			this.user_fullname = user_fullname;
		}
		public String getEmailid() {
			return emailid;
		}
		public void setEmailid(String emailid) {
			this.emailid = emailid;
		}
		public long getMobileno() {
			return mobileno;
		}
		public void setMobileno(long mobileno) {
			this.mobileno = mobileno;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getConformpassword() {
			return conformpassword;
		}
		public void setConformpassword(String conformpassword) {
			this.conformpassword = conformpassword;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		
		public UserReg() {
			super();
		}
		public UserReg(Integer userregid, String user_fullname, String emailid, long mobileno, String password,
				String conformpassword, String address, String gender) {
			super();
			this.userregid = userregid;
			this.user_fullname = user_fullname;
			this.emailid = emailid;
			this.mobileno = mobileno;
			this.password = password;
			this.conformpassword = conformpassword;
			this.address = address;
			this.gender = gender;
		}
		public UserReg(Integer userregid) {
			super();
			this.userregid = userregid;
		}
		@Override
		public String toString() {
			return "UserReg [userregid=" + userregid + ", user_fullname=" + user_fullname + ", emailid=" + emailid
					+ ", mobileno=" + mobileno + ", password=" + password + ", conformpassword=" + conformpassword
					+ ", address=" + address + ", gender=" + gender + "]";
		}
		
		
		
	    


}
