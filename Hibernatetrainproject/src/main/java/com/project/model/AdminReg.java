package com.project.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;

@Entity
public class AdminReg {
	
	    @Id
	    @Column
	    Integer adminregid;
        String ad_fullname;
        @Column
       // @ApiModelProperty("this is for the emailid of the AdminReg")
       // @Email(message = "@ is mandatory")
        String emailid;
        @Column
        @Size(min=8,message="The Password Operation at least 8 characters")
        @Size(max=15,message="The Password Operation at most 15 characters")
       // @ApiModelProperty(notes = "this for the password of the AdminReg")
        String password;
        @Column
        @Size(min=8,message="The Password Operation at least 8 characters")
        @Size(max=15,message="The Password Operation at most 15 characters")
		String conformpassword;
		String address;
		String gender;
		
		
		
		public Integer getAdminregid() {
			return adminregid;
		}
		public void setAdminregid(Integer adminregid) {
			this.adminregid = adminregid;
		}
		public String getAd_fullname() {
			return ad_fullname;
		}
		public void setAd_fullname(String ad_fullname) {
			this.ad_fullname = ad_fullname;
		}
		public String getEmailid() {
			return emailid;
		}
		public void setEmailid(String emailid) {
			this.emailid = emailid;
		}

		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getConformpassword() {
			return conformpassword;
		}
		public void setConformpassword(String conformpassword) {
			this.conformpassword = conformpassword;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		
		public AdminReg() {
			super();
		}
		public AdminReg(Integer adminregid, String ad_fullname, String emailid, String password, String conformpassword,
				String address, String gender) {
			super();
			this.adminregid = adminregid;
			this.ad_fullname = ad_fullname;
			this.emailid = emailid;
			this.password = password;
			this.conformpassword = conformpassword;
			this.address = address;
			this.gender = gender;
		}
		public AdminReg(Integer adminregid) {
			super();
			this.adminregid = adminregid;
		}
		@Override
		public String toString() {
			return "AdminReg [adminregid=" + adminregid + ", ad_fullname=" + ad_fullname + ", emailid=" + emailid
					+ ", password=" + password + ", conformpassword=" + conformpassword + ", address=" + address
					+ ", gender=" + gender + "]";
		}
		

		
		

}
