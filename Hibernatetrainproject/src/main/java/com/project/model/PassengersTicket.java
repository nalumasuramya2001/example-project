package com.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;



@Entity
public class PassengersTicket {
	 
	    @Id
        Integer passengerid;
	    String passenger_name;
	    String source;
	    String destination;
	    String selected_seattype;
	    int selected_passengers;
		
	    public Integer getPassengerid() {
			return passengerid;
		}
		public void setPassengerid(Integer passengerid) {
			this.passengerid = passengerid;
		}
		public String getPassenger_name() {
			return passenger_name;
		}
		public void setPassenger_name(String passenger_name) {
			this.passenger_name = passenger_name;
		}
		public String getSource() {
			return source;
		}
		public void setSource(String source) {
			this.source = source;
		}
		public String getDestination() {
			return destination;
		}
		public void setDestination(String destination) {
			this.destination = destination;
		}
		public String getSelected_seattype() {
			return selected_seattype;
		}
		public void setSelected_seattype(String selected_seattype) {
			this.selected_seattype = selected_seattype;
		}
		public int getSelected_passengers() {
			return selected_passengers;
		}
		public void setSelected_passengers(int selected_passengers) {
			this.selected_passengers = selected_passengers;
		}
		public PassengersTicket(Integer passengerid, String passenger_name, String source, String destination,
				String selected_seattype, int selected_passengers) {
			super();
			this.passengerid = passengerid;
			this.passenger_name = passenger_name;
			this.source = source;
			this.destination = destination;
			this.selected_seattype = selected_seattype;
			this.selected_passengers = selected_passengers;
		}
		public PassengersTicket() {
			super();
		}
		public PassengersTicket(Integer passengerid) {
			super();
			this.passengerid = passengerid;
		}
		@Override
		public String toString() {
			return "PassengersTicket [passengerid=" + passengerid + ", passenger_name=" + passenger_name + ", source="
					+ source + ", destination=" + destination + ", selected_seattype=" + selected_seattype
					+ ", selected_passengers=" + selected_passengers + "]";
		}
		
	    
	    
		
	   

}
