package com.project.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.stereotype.Component;

@Entity
public class Train {
	@Id
	Integer trainno;
	String trainname;
	@Column
	String source;
	@Column
	String destination;
	String journeystarttime;
	String seattype ;//use select method
	int ac1seatcost;
	int ac2seatcost;
	int sleeper_seatcost;
	int general_seatcost;





	public Integer getTrainno() {
		return trainno;
	}

	public void setTrainno(Integer trainno) {
		this.trainno = trainno;
	}

	public String getTrainname() {
		return trainname;
	}
	public void setTrainname(String trainname) {
		this.trainname = trainname;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getJourneystarttime() {
		return journeystarttime;
	}

	public void setJourneystarttime(String journeystarttime) {
		this.journeystarttime = journeystarttime;
	}
	public String getSeattype() {
		return seattype;
	}
	public void setSeattype(String seattype) {
		this.seattype = seattype;
	}
	public int getAc1seatcost() {
		return ac1seatcost;
	}
	public void setAc1seatcost(int ac1seatcost) {
		this.ac1seatcost = ac1seatcost;
	}
	public int getAc2seatcost() {
		return ac2seatcost;
	}
	public void setAc2seatcost(int ac2seatcost) {
		this.ac2seatcost = ac2seatcost;
	}
	public int getGeneral_seatcost() {
		return general_seatcost;
	}
	public void setGeneral_seatcost(int general_seatcost) {
		this.general_seatcost = general_seatcost;
	}
	public int getSleeper_seatcost() {
		return sleeper_seatcost;
	}
	public void setSleeper_seatcost(int sleeper_seatcost) {
		this.sleeper_seatcost = sleeper_seatcost;
	}

	public Train() {
		super();
	}

  public Train(Integer trainno) {
		super();
		this.trainno = trainno;
	}
  

	public Train(Integer trainno, String trainname, String source, String destination, String journeystarttime,
		String seattype, int ac1seatcost, int ac2seatcost, int sleeper_seatcost, int general_seatcost) {
	super();
	this.trainno = trainno;
	this.trainname = trainname;
	this.source = source;
	this.destination = destination;
	this.journeystarttime = journeystarttime;
	this.seattype = seattype;
	this.ac1seatcost = ac1seatcost;
	this.ac2seatcost = ac2seatcost;
	this.sleeper_seatcost = sleeper_seatcost;
	this.general_seatcost = general_seatcost;
}

	@Override
	public String toString() {
		return "Train [trainno=" + trainno + ", trainname=" + trainname + ", source=" + source + ", destination="
				+ destination + ", journeystarttime=" + journeystarttime + ", seattype=" + seattype + ", ac1seatcost="
				+ ac1seatcost + ", ac2seatcost=" + ac2seatcost + ", sleeper_seatcost=" + sleeper_seatcost
				+ ", general_seatcost=" + general_seatcost + "]";
	}


	
	

    


}
