package com.project.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class NewPayment {

	@Id
    Integer paymentid;
	Long cardnumber;
	String expirydate;
	int cvv;
   
	public Integer getPaymentid() {
		return paymentid;
	}
	public void setPaymentid(Integer paymentid) {
		this.paymentid = paymentid;
	}
	public Long getCardnumber() {
		return cardnumber;
	}
	public void setCardnumber(Long cardnumber) {
		this.cardnumber = cardnumber;
	}
	public String getExpirydate() {
		return expirydate;
	}
	public void setExpirydate(String expirydate) {
		this.expirydate = expirydate;
	}
	public int getCvv() {
		return cvv;
	}
	public void setCvv(int cvv) {
		this.cvv = cvv;
	}
	public NewPayment() {
		super();
	}
	public NewPayment(Integer paymentid) {
		super();
		this.paymentid = paymentid;
	}
	public NewPayment(Integer paymentid, Long cardnumber, String expirydate, int cvv) {
		super();
		this.paymentid = paymentid;
		this.cardnumber = cardnumber;
		this.expirydate = expirydate;
		this.cvv = cvv;
	}
	@Override
	public String toString() {
		return "NewPayment [paymentid=" + paymentid + ", cardnumber=" + cardnumber + ", expirydate=" + expirydate
				+ ", cvv=" + cvv + "]";
	}
	
	
}
