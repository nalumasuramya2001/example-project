package com.project.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.project.model.Members;
import com.project.model.Train;

public interface Memberrepository extends JpaRepository<Members,Integer>{

}
