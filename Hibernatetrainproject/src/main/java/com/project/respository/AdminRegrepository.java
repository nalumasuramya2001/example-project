package com.project.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.model.AdminReg;
import com.project.model.Train;

@Repository
public interface AdminRegrepository extends JpaRepository<AdminReg,Integer> {
	  
	public static final String email_pass="select * from admin_reg where emailid=?1 and password=?2";
	@Query(value=email_pass,nativeQuery=true)
	public AdminReg findByemailidpassword(String emailid, String password);
	
	public static final String email_pass1="select * from admin_reg where emailid=?1 and password=?2";
	@Query(value=email_pass1,nativeQuery=true)
	public AdminReg findByemailid(String emailid,String password);
	
	

	
	
	
	
	
	

}
