package com.project.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.project.model.AdminReg;
import com.project.model.UserReg;


@Repository
public interface UserRegrepository extends JpaRepository<UserReg,Integer>{

	
	public static final String email_passreg="select * from user_reg where emailid=?1 and password=?2";
	@Query(value=email_passreg,nativeQuery=true)
	public AdminReg findByuseremailidpassword(String emailid, String password);
	
	
	public static final String email_passreg1="select * from user_reg where emailid=?1 and password=?2";
	@Query(value=email_passreg1,nativeQuery=true)
	public UserReg findByemailid(String emailid, String password);
}
