package com.project.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.NewPayment;

@Repository
public interface NewPaymentrepository extends JpaRepository<NewPayment,Integer> {

}
