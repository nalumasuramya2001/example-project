package com.project.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.model.AdminReg;
import com.project.model.PassengersTicket;


@Repository
public interface PassengerTicketrepository extends JpaRepository<PassengersTicket,Integer> {

}
