<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>TRAIN UPDATE</title>
<link rel="stylesheet" href="style.css">
</head>
<body>
 <center style="margin-left:-50px;">
            <form action="updatetrain" method="post">
            <input type="hidden" name="_method" value="put">

        
         <div style="display: grid; height: 820px; width: 100%; background-color: rgb(70, 68, 68);">
            
        <div class="wrapper" style= "width:190%;">
            <div class="title-text">
               <div class="title login" style="margin-left:0px; color:rgb(47, 0, 255); font-style:bold; font-size:30px ;">
                 UPDATE TRAIN
               </div><br>
            </div>

               <div class="form-inner">
                   <div class="field">
                    <div style="margin-left:-280px;">
                    trainno:<br></div>
                   <input type="text" placeholder="train number" name="trainno" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                   </div><br>
                  
                   <div class="field">
                    <div style="margin-left:-258px;">
                     trainname:<br></div>
                   <input type="text" placeholder="train name" name="trainname" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                   </div><br>
                  
                   <div class="field">
                    <div style="margin-left:-279px;">
                        source:<br></div>
                    <input type="text" placeholder="your starting location" name="source" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                   </div><br>
                   <div class="field">
                    <div style="margin-left:-244px;">
                    Destination:<br></div>
                    <input type="text" placeholder="Your Destination location" name="destination" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                   </div><br>
                     <div class="field">
                    <div style="margin-left:-215px;">
                    Journeystarttime:<br></div>
                    <input type="text" placeholder="journeystarttime" name="journeystarttime" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                   </div><br>
                   <div class="field">
                    <div style="margin-left:-269px;">
                        seattype:<br></div>
            <div style="margin-left:-130px;">
          <a><input type="checkbox" name="seattype" value="AC1" style="margin-left:70px;">AC1SEATS</a> 
           <a><input type="checkbox" name="seattype" value="AC2" style="margin-left:80px;">AC2SEATS<br></a> 
             <a><input type="checkbox" name="seattype"  value="SLEEPER" style="margin-left:64px;">SLEEPER</a> 
            <a><input type="checkbox" name="seattype"  value="GENERAL"  style="margin-left:90px;">GENERAL<br></a> 
        </div>
                   </div><br>
                
                   <div class="field">
                    <div style="margin-left:-237px;">
                        Ac1seatcost:</div>
                        <input type="number" placeholder="Ac1seatcost"  name="ac1seatcost" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                  </div><br>
                  <div class="field">
                    <div style="margin-left:-237px;">
                        Ac2seatcost:</div>
                       <input type="number" placeholder="Ac2seatcost"  name="ac2seatcost" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                  </div><br>
                  <div class="field">
                    <div style="margin-left:-220px;">
                        sleeper_seatcost:</div>
                       <input type="number" placeholder="sleeper_seatcost"  name="sleeper_seatcost" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                  </div><br>
                  <div class="field">
                    <div style="margin-left:-220px;">
                       general_seatcost:</div>
                       <input type="number" placeholder="general_seatcost"  name="general_seatcost" style="background-size:contain; height:30px; width:100%; border-color: black;" required>
                  </div><br>
                  
               
                   
                <div class="field btn">
                      <div class="btn-layer">
                        <button value="SUBMIT"  style="background-size:contain; height:30px; font-size: 18px; width:100%; background-color: rgb(0, 174, 255); margin-left:0px; border-color: black;">Submit</button>
                        
                      
                      
                      </div><br><br>
                      

                  </div>
                </form>   
              </center>
           
</body>
</html>