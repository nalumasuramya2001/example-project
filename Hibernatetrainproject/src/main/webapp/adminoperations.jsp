<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>ADMIN OPERATIONS</title>

<link rel="stylesheet" href="design.css">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">  
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"> </script>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font awesome/4.7.0/css/font-awesome.min.css">
</head>
    <style>
    body{
        background-image: url("https://lh3.googleusercontent.com/proxy/a_xH40rEnF9iCY2cZ9d2kLtXnCTJyulp_24Jzg2ZVVjyQvn6qh4OO1V8Hp2jOeTIF2ivI8wun5_39wsQldDQ3v9ksUWlsDel6EpSeKBJBNELDzgrClBThJrEXApIaac9MOL-A73j-275xV7jFWrbBA=w1200-h630-p-k-no-nu");
        background-size:1800px;
    }
</style>
<body>
<section id="nav-bar">

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#" style="margin-left:20px; color:white"> TRAIN</a>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="addtrainrequest">ADD TRAINS <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="trainupdaterequest">UPDATE TRAIN </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="deletetrainrequest">DELETE TRAIN </a>
            </li> 
            <li class="nav-item">
              <a class="nav-link " href="totaltrainlistrequest">VIEW TRAINS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="ticketlistreq">VIEW TICKETS </a>
            </li>
          </ul>
        </div>
      </nav>
 </section>
<section id="banner"> 
<div class="container">

 <div class="row">

<div class="col-md-6">

<p class="promo-title">ADMIN ACTIVITIES</p>

<p>Admin can add train,update train,delete train,view trains and view tickets.</p> 

</div>

<div class="col-md-6 text-center">




</div>

</div>

</div>
<p style="color:pink;">.</p>

</body>
</html>