<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>VIEW TRAINS</title>
</head>
<body style="background-color:rgb(44, 39, 39);"><br>
 <form action="/gettrainlist" method="get" > 
            <div class="table-container">
             <center><h1 class="heading"  style="color:rgb(25, 212, 72);">***VIEW TRAINS***</h1> </center><br><br><br>
        <table  border="1" cellspacing="5" cellpadding="5" bgcolor="white" bordercolor="black" style="margin-left:60px; ">
            <thead>
            <tr>
                <th >Trainno</th>
                <th>Trainname</th>
                <th>Source</th>
                <th>Destination</th>
                 <th>JourneyStartTime</th>
                <th>Seattype</th>
               <th>Ac1seatcost</th>
                  <th>#</th>
                 <th>Ac2seatcost</th>
                   <th>#</th>
                <th>Sleeper_seatcost</th>
                  <th>#</th>
                <th>General_seatcost</th>
                  <th>#</th>
            </tr>
  
        </thead> 
        <tbody>
            <c:forEach items="${listtrain}" var="tn1">
            <tr>
                <td>${tn1.trainno}</td>
                <td>${tn1.trainname}</td>
                <td>${tn1.source}</td>
                <td>${tn1.destination}</td>
                 <td>${tn1.journeystarttime}</td>
                <td>${tn1.seattype}</td>
              <td>${tn1.ac1seatcost}</td>
                <td><button style="height:30px;">BOOK</button>
                <td>${tn1.ac2seatcost}</td>
                 <td><button style="height:30px;">BOOK</button>
                <td>${tn1.sleeper_seatcost}</td>
                 <td><button style="height:30px;">BOOK</button>
                <td>${tn1.general_seatcost}</td>
                 <td><button style="height:30px;">BOOK</button>
              </tr>
        </c:forEach>
    </tbody>
 
        </table>
  

        </form>
        
</body>
</html>