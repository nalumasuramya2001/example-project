package com.project.testcontroller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.controller.AdminRegcontroller;
import com.project.model.AdminReg;
import com.project.respository.AdminRegrepository;
import com.project.service.AdminRegservice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Testadminregcontroller {
	
	@Autowired
	AdminRegcontroller adminregcontroller;

	@MockBean
	AdminRegservice adminRegservice;

	   @Test
	   public void adminregreqtest() {
		   AdminReg adminreg=new AdminReg(4,"adhbutha","adhbutha@gmail.com","adhbutha987","adhbutha987","Telangana","female"); 
		   equals(adminregcontroller. adminregistrationrequest());
	   }
	   @Test
	   public void addadminregtest() {
		   AdminReg adminreg=new AdminReg(4,"adhbutha","adhbutha@gmail.com","adhbutha987","adhbutha987","Telangana","female"); 
		   equals(adminregcontroller.addadminregistration(adminreg));
	   }
	   
	   @Test
	   public void adminlogreqtest() {
		   AdminReg adminreg=new AdminReg(4,"adhbutha","adhbutha@gmail.com","adhbutha987","adhbutha987","Telangana","female"); 
		   equals(adminregcontroller.adminlogrequest());
	   }
	   @Test
	   public void contacttest() {
		   equals(adminregcontroller.contact());
	   }
	   
}
