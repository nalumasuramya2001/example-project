package com.project.testservice;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.project.HibernatetrainprojectApplication;
import com.project.model.AdminReg;
import com.project.model.Train;
import com.project.respository.AdminRegrepository;
import com.project.service.AdminRegservice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class TestAdminRegservice {


	@Autowired
	AdminRegservice adminRegservice;

	@MockBean
	AdminRegrepository adminregrepo;
	


    @Test	
	public void addadminregTest() throws Exception {
		AdminReg adminreg=new AdminReg(4,"adhbutha","adhbutha@gmail.com","adhbutha987","adhbutha987","Telangana","female");
		when(adminregrepo.save(adminreg)).thenReturn(adminreg);
		 equals(adminRegservice.addadminregistration(adminreg));
		}
   @Test
   public void adminregreqtest() {
	   AdminReg adminreg=new AdminReg(4,"adhbutha","adhbutha@gmail.com","adhbutha987","adhbutha987","Telangana","female"); 
	   equals(adminRegservice.adminregistrationrequest());
   }
   
   @Test
   public void adminlogreqtest() {
	   AdminReg adminreg=new AdminReg(4,"adhbutha","adhbutha@gmail.com","adhbutha987","adhbutha987","Telangana","female"); 
	   equals(adminRegservice.adminlogrequest());
   }
 

}



