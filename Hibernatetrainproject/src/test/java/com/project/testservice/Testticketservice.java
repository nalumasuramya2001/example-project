package com.project.testservice;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.NewPayment;
import com.project.model.PassengersTicket;
import com.project.model.Train;
import com.project.respository.PassengerTicketrepository;
import com.project.respository.UserRegrepository;
import com.project.service.PassengersTicketservice;
import com.project.service.UserRegservice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Testticketservice {

	@Autowired
	PassengersTicketservice passengerservice;

	@MockBean
	PassengerTicketrepository passengerrepository;
	
    @Test
    public void  paymentreqtest() {
    PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	equals(passengerservice.addpassengerticketrequest());
  }
    
	@Test
	public void addpassengertickettest() {
	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	 when(passengerrepository.save(passengerticket)).thenReturn(passengerticket);
	 equals(passengerservice.addpassengerticket(passengerticket));
}
	    @Test
	    public void  getticketlistreqtest() {
	    Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		equals(passengerservice.ticketlistreq());
	    }

	
	@Test
	 public void getticketlist() throws Exception {
		 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
         when(passengerrepository.findAll()).thenReturn(Stream
		 .of(new PassengersTicket(1,"simmi","hyd","delhi","ac1",4),new PassengersTicket(1,"simmi","hyd","delhi","ac1",4))
		 .collect(Collectors.toList()));
	     equals(passengerservice.getticketlist(passengerticket));
     }
    
	@Test
    public void  dltticketreqtest() {
    PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	equals(passengerservice.dltticketreq());
    }
	
    @Test
	public void deleteUserTest() {
    	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
    	 int passengerid = 1;
    	 passengerservice.dltticket(passengerid);
		 verify(passengerrepository, times(1)).deleteById(passengerid);
	}
    

}
