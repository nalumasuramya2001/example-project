package com.project.testservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.project.HibernatetrainprojectApplication;
import com.project.model.Members;
import com.project.model.NewPayment;
import com.project.respository.NewPaymentrepository;
import com.project.service.NewPaymentservice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Testpaymentservice {
	@Autowired
	NewPaymentservice paymentservice;

	@MockBean
	NewPaymentrepository paymentrepository;
	
    MockMvc mocmvc;
	
    @Test
	public void addpaymentTest() throws Exception {
    	NewPayment newpayment=new NewPayment(1,9876543L,"12/12/2022",133);
    	when(paymentrepository.save(newpayment)).thenReturn(newpayment);
    	 equals(paymentservice.addpayment(newpayment));
    	}
    
      @Test
      public void  paymentrequest() {
      NewPayment newpayment=new NewPayment(1,9876543L,"12/12/2022",133);
 	  equals(paymentservice.paymentrequest());
    }
}

