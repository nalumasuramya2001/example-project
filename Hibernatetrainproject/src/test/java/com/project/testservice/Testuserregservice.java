package com.project.testservice;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.Train;
import com.project.model.UserReg;
import com.project.respository.UserRegrepository;
import com.project.service.UserRegservice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Testuserregservice {
	@Autowired
	UserRegservice userregservice;

	@MockBean
	UserRegrepository userregrepo;
	
	
    @Test
   public void  adduserreqtest() {
    UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
	equals(userregservice.userregistrationrequest());
   }

    @Test
	public void adduserTest() {
    	UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		when(userregrepo.save(userreg)).thenReturn(userreg);
		equals( userregservice.adduserreg(userreg));
		}
    @Test
   public void  userlogreqtest() {
    UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
	equals(userregservice.userlogrequest());
   }
}
