package com.project.testservice;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.project.HibernatetrainprojectApplication;
import com.project.model.PassengersTicket;
import com.project.model.Train;
import com.project.respository.Trainrepository;
import com.project.service.Trainservice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class TestTrainservice {
	@Autowired
	Trainservice trainservice;

	@MockBean
	Trainrepository trainrepo;
	
	
	     @Test
	    public void  trainlistreqtest() {
	    Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		equals(trainservice.totaltrainlistrequest());
	    }
	 @Test
	 public void gettrainTest() throws Exception {
	  Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
	
	  when(trainrepo.findAll()).thenReturn(Stream
			  .of(new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700),
			  new Train(2,"trainname","source","destination","8:00am","seattype",1000,900,800,700)).collect(Collectors.toList()));
	  equals(trainservice.gettrainlist(train));
      }

    @Test
    public void  addtrainreqtest() {
    Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
	equals(trainservice.addtrainrequest());
  }
    @Test
	public void addtrainTest() throws Exception {
    	Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		when(trainrepo.save(train)).thenReturn(train);
		equals(trainservice.addtrain(train));
		}
    @Test
    public void  dlttrainreqtest() {
    Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
	equals(trainservice.deletetrainrequest());
  }
    @Test
	public void deleteUserTest() {
    	Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
    	int trainno = 1;
    	trainservice.dlttrain(trainno);
		verify(trainrepo, times(1)).deleteById(trainno);
	}
    
    @Test
    public void  updatetrainreqtest() {
    Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
	equals(trainservice.trainupdaterequest());
  }
    
    @Test
    public void  searchrequest() {
    Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
	equals(trainservice.request4());
  }
}