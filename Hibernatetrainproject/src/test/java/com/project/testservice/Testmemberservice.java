package com.project.testservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.project.HibernatetrainprojectApplication;
import com.project.model.AdminReg;
import com.project.model.Members;
import com.project.respository.Memberrepository;
import com.project.service.Memberservice;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Testmemberservice {
	
	@Autowired
	Memberservice memberservice;

	@MockBean
	Memberrepository memberrepo;
	

	
    @Test
	public void addmembersTest() {
    	Members members=new Members(2,2);
        when(memberrepo.save(members)).thenReturn(members);
        equals(memberservice.addmembers(members));
    }
    @Test
    public void membersrequesttest() {
    	Members members=new Members(2,2);
 	   equals(memberservice.membersrequest());
    }
}
