package com.project.testmodel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.UserReg;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class UserRegtest {

	@MockBean
	 UserReg userreg;
	
	@Test
	public void testuserregid() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setUserregid(121);
		assertEquals(true,userreg.getUserregid()==121);
	}
	@Test
	public void testuserregusername() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setUser_fullname("adhbutha");
		assertEquals(true,userreg.getUser_fullname()=="adhbutha");
	}

	@Test
	public void testuserregemailid() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setEmailid("adhbutha@gmail.com");
		assertEquals(true,userreg.getEmailid()=="adhbutha@gmail.com");
	}
	@Test
	public void testuserregmobileno() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setMobileno(994967);
		assertEquals(true,userreg.getMobileno()==994967);
	}
	@Test
	public void testuserpassword() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setPassword("adhbutha987");
		assertEquals(true,userreg.getPassword()=="adhbutha987");
	}
	@Test
	public void testuserConformpassword() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setConformpassword("adhbutha987");
		assertEquals(true,userreg.getConformpassword()=="adhbutha987");
	}
	@Test
	public void testadminAddress() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setAddress("Telangana");
		assertEquals(true,userreg.getAddress()=="Telangana");
	}
	@Test
	public void testuserGender() {
		UserReg userreg=new UserReg(1,"admin_name","email@gmail.com",9199,"password","conformpassword","address","gender");
		userreg.setGender("female");
		assertEquals(true,userreg.getGender()=="female");
	}
	@Test
	public void testuserregsingleconstructor() {
		UserReg userreg=new UserReg();
	}
	@Test
	public void testuserregconstructorregid() {
		UserReg userreg=new UserReg(1);
	}
}
