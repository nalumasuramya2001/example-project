package com.project.testmodel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.NewPayment;
import com.project.model.Train;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Paymenttest {
	
	@MockBean
	NewPayment newpayment;
	
	@Test
	public void testpaymentid() {
		NewPayment newpayment=new NewPayment(1,9876543L,"12/12/2022",133);
		newpayment.setPaymentid(1);
		assertEquals(true,newpayment.getPaymentid()==1);
	}
	@Test
	public void testCardnumber() {
		NewPayment newpayment=new NewPayment(1,9876543L,"12/12/2022",133);
		newpayment.setCardnumber(1234L);
		assertEquals(true,newpayment.getCardnumber()==1234L);
	}

	@Test
	public void testExpirydate() {
		NewPayment newpayment=new NewPayment(1,9876543L,"12/12/2022",133);
		String date="12/12/2022";
		newpayment.setExpirydate(date);
		assertEquals(true,newpayment.getExpirydate().equals(date));
	}
	@Test
	public void testCvv() {
		NewPayment newpayment=new NewPayment(1,9876543L,"12/12/2022",133);
		newpayment.setCvv(133);
		assertEquals(true,newpayment.getCvv()==133);
	}
	@Test
	public void testpaymentconstructor() {
		NewPayment newpayment=new NewPayment();
	}
	@Test
	public void testpaymentconstructorpaymentid() {
		NewPayment newpayment=new NewPayment(1);
	}

	
}
