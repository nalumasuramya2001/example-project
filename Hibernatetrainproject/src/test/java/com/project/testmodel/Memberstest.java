package com.project.testmodel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.Members;
import com.project.model.Train;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Memberstest {

	
	@MockBean
	Members members;
	
	@Test
	public void testAdults() {
		Members members=new Members(2,2);
		int adults=2;
		members.setAdults(adults);
		assertEquals(true,members.getAdults()==(adults));
	}
	@Test
	public void testChildren() {
		Members members=new Members(2,2);
		members.setChildren(2);
		assertEquals(true,members.getChildren()==2);
	}
	@Test
	public void testConstructor() {
		Members members=new Members();
}
	
	
}