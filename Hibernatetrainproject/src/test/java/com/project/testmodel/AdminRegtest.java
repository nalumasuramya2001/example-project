package com.project.testmodel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.AdminReg;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class AdminRegtest {
	@MockBean
	 AdminReg adminreg;
	
	@Test
	public void testadminregid() {
		AdminReg adminreg=new AdminReg(1,"admin_name","email@gmail.com","password","conformpassword","address","gender");
		int regid=121;
		adminreg.setAdminregid(regid);
		assertEquals(true,adminreg.getAdminregid().equals(regid));
	}
	@Test
	public void testadminregadminname() {
		AdminReg adminreg=new AdminReg(1,"admin_name","email@gmail.com","password","conformpassword","address","gender");
		String name="adhbutha";
		adminreg.setAd_fullname(name);
		assertEquals(true,adminreg.getAd_fullname().equals(name));
	}

	@Test
	public void testadminregemailid() {
		AdminReg adminreg=new AdminReg(1,"admin_name","email@gmail.com","password","conformpassword","address","gender");
		String email="adhbutha@gmail.com";
		adminreg.setEmailid(email);
		assertEquals(true,adminreg.getEmailid().equals(email));
	}
	@Test
	public void testadminpassword() {
		AdminReg adminreg=new AdminReg(1,"admin_name","email@gmail.com","password","conformpassword","address","gender");
		String password="adhbutha987";
		adminreg.setPassword(password);
		assertEquals(true,adminreg.getPassword().equals(password));
	}
	@Test
	public void testadminConformpassword() {
		AdminReg adminreg=new AdminReg(1,"admin_name","email@gmail.com","password","conformpassword","address","gender");
		String conformpassword="adhbutha987";
		adminreg.setConformpassword(conformpassword);
		assertEquals(true,adminreg.getConformpassword().equals(conformpassword));
	}
	@Test
	public void testadminAddress() {
		AdminReg adminreg=new AdminReg(1,"admin_name","email@gmail.com","password","conformpassword","address","gender");
		String address="Telangana";
		adminreg.setAddress(address);
		assertEquals(true,adminreg.getAddress().equals(address));
	}
	@Test
	public void testadminGender() {
		AdminReg adminreg=new AdminReg(1,"admin_name","email@gmail.com","password","conformpassword","address","gender");
		String gender="female";
		adminreg.setGender(gender);
		assertEquals(true,adminreg.getGender().equals(gender));
	}
	@Test
	public void testadmincontructor() {
		AdminReg adminreg=new AdminReg();
     }
	@Test
	public void testadmincontructoradminregid() {
		AdminReg adminreg=new AdminReg(1);
   }
}
