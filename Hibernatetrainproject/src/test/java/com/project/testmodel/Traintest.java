package com.project.testmodel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.Train;


@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class Traintest {

	@MockBean
	Train train;
	
	@Test
	public void testtrainno() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		train.setTrainno(121);
		assertEquals(true,train.getTrainno()==121);
	}
	@Test
	public void testtrainname() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		String trainname="express";
		train.setTrainname(trainname);
		assertEquals(true,train.getTrainname().equals(trainname));
	}

	@Test
	public void testsource() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		String source="Hyd";
		train.setSource(source);
		assertEquals(true,train.getSource().equals(source));
	}
	@Test
	public void testdestination() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		String destination="Dmp";
		train.setDestination(destination);
		assertEquals(true,train.getDestination().equals(destination));
	}
	
	@Test
	public void testjourneystarttime() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		String starttime="8:00am";
		train.setJourneystarttime(starttime);
		assertEquals(true,train.getJourneystarttime().equals(starttime));
	}
	
	@Test
	public void testseattype() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		String seattype="seattype";
		train.setSeattype(seattype);
		assertEquals(true,train.getSeattype().equals(seattype));
	}
	@Test
	public void testAc1seatcost() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		train.setAc1seatcost(1000);
		assertEquals(true,train.getAc1seatcost()==1000);
	}
	@Test
	public void testAc2seatcost() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		train.setAc2seatcost(900);
		assertEquals(true,train.getAc2seatcost()==900);
	}
	@Test
	public void testsleeperseatcost() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		train.setSleeper_seatcost(800);
		assertEquals(true,train.getSleeper_seatcost()==800);
	}
	@Test
	public void testgeneralseatcost() {
		Train train=new Train(1,"trainname","source","destination","8:00am","seattype",1000,900,800,700);
		train.setGeneral_seatcost(700);
		assertEquals(true,train.getGeneral_seatcost()==700);
	}
	@Test
	public void testtrainsingleconstructor() {
		Train train=new Train();
	}
	@Test
	public void testtrainconstructortrainno() {
		Train train=new Train(1);
	}
}
