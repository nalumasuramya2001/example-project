package com.project.testmodel;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.project.HibernatetrainprojectApplication;
import com.project.model.PassengersTicket;

@RunWith(SpringRunner.class)
@SpringBootTest(classes=HibernatetrainprojectApplication.class)
public class PassengerTicketTest {

	@Test
	public void passengeridtest() {
	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	 int id=1;
	 passengerticket.setPassengerid(id);
	 assertEquals(true, passengerticket.getPassengerid()==id);
	}
	
	@Test
	public void passengernametest() {
	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	 String name="simmi";
	 passengerticket.setPassenger_name(name);
	 assertEquals(true, passengerticket.getPassenger_name().equals(name));
	}
	
	@Test
	public void passengerfromtest() {
	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	 String from="hyd";
	 passengerticket.setSource(from);
	 assertEquals(true, passengerticket.getSource().equals(from));
	}
	
	@Test
	public void passengerTotest() {
	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	 String To="delhi";
	 passengerticket.setDestination(To);
	 assertEquals(true, passengerticket.getDestination().equals(To));
	}
	
	@Test
	public void passengerseattest() {
	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	 String seat="ac1";
	 passengerticket.setSelected_seattype(seat);
	 assertEquals(true, passengerticket.getSelected_seattype().equals(seat));
	}
	
	@Test
	public void passengerpassengerstest() {
	 PassengersTicket passengerticket =new PassengersTicket(1,"simmi","hyd","delhi","ac1",4);
	 int passengers=4;
	 passengerticket.setSelected_passengers(passengers);
	 assertEquals(true, passengerticket.getSelected_passengers()==(passengers));
	}
	
	@Test
	public void singleidconstructor() {
		 PassengersTicket passengerticket =new PassengersTicket(1);
	}
	@Test
	public void singleconstructor() {
		 PassengersTicket passengerticket =new PassengersTicket();
	}
}
